def create_markdown_table(headers, rows)
  header_row = "| " + headers.join(" | ") + " |"
  
  separator_row = "| " + headers.map { '-' * 5 }.join(" | ") + " |" 
  
  markdown_table = [header_row, separator_row]

  rows.each do |row|
    formatted_row = "| " + row.map(&:to_s).join(" | ") + " |"
    markdown_table << formatted_row
  end

  markdown_table.join("\n")
end

def inspect_tool(cmd)
  stdout, stderr, status = Open3.capture3(cmd)

  if status.success?
    return stdout.strip.gsub("\n", " ")
  else
    return stderr.strip.gsub("\n", " ")
  end
rescue => e
  return e
end

def fastlane_version
  cmd = "fastlane --version"
  regex = /fastlane \d+\.\d+\.\d+/
  stdout, stderr, status = Open3.capture3(cmd)

  if status.success?
    return stdout.lines.find { |string| string.match?(regex) }.strip
  else
    return stderr.strip.gsub("\n", " ")
  end
rescue => e
  return e
end

def sdks_to_markdown(data)
  sdk_data = {}
  current_sdk_type = nil
  
  data.lines.each do |line|
    if line.strip.empty?
      next
    elsif line.strip.end_with?("SDKs:")
      current_sdk_type = line.strip
      sdk_data[current_sdk_type] = []
    else
      sdk_details = line.strip.split("\t").map(&:strip)
      sdk_data[current_sdk_type] << sdk_details
    end
  end
  
  markdown_output = []
  markdown_output << "| SDK Version | Command |"
  markdown_output << "| ----------- |---------|"
  sdk_data.each do |sdk_type, sdks|
    markdown_output << "| **#{sdk_type}** | |"
    sdks.each do |sdk|
      markdown_output << "| #{sdk[0]} | `#{sdk[1]}` |"
    end
  end
  
  markdown_output.join("\n")
end

def simulators_to_markdown
  command = "xcrun simctl list --json devices available"
  output, stderr, status = Open3.capture3(command)

  return unless status.success?

  data = JSON.parse(output)

  transformed_devices = data["devices"].map do |runtime, devices|
    if match = runtime.match(/com\.apple\.CoreSimulator\.SimRuntime\.iOS-(.+)/)
      version = match[1].gsub('-', '.')
      devices.map { |device| { name: "#{device['name']} (#{version})", udid: device['udid'] } }
    else
      []
    end
  end.flatten

  markdown_output = []
  markdown_output << "| Name | UDID |"
  markdown_output << "| ----------- |---------|"

  transformed_devices.each do |device|
    markdown_output << "| #{device[:name]} | #{device[:udid]} |"
  end

  markdown_output.join("\n")
end

def homebrew_to_markdown
  command = "brew config"
  output, stderr, status = Open3.capture3(command)

  return unless status.success?

  markdown_output = []
  markdown_output << "| Option | Value |"
  markdown_output << "| ----------- |---------|"

  output.lines.each do |line|
    parts = line.split(":")
    markdown_output << "| #{parts[0]} | `#{parts[1].strip.gsub("\n", " ")}` |"
  end

  markdown_output.join("\n")
end