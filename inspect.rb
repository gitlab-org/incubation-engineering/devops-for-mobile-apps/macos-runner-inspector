#!/bin/ruby

require 'yaml'
require 'json'
require 'open3'
require './functions.rb'

tools = YAML.load_file('inspections.yml')

headers = ['name', 'command', 'output']
rows = []

tools.each do |tool|
  if tool['name'] == 'fastlane'
    version = fastlane_version
  else
    version = inspect_tool(tool['command'])
  end

  rows << [tool['name'], tool['command'], version]
end

puts "---"
puts "date: #{Time.new.strftime("%Y-%m-%dT%H:%M:%S%:z")}"
puts "title: #{ENV['IMAGE']} Configuration"
puts "---"

markdown = create_markdown_table(headers, rows)
puts markdown

puts "\n\n## Installed SDKs\n\n"

sdk_table = sdks_to_markdown(`xcodebuild -showsdks`)

puts sdk_table

puts "\n\n## Installed Simulators\n\n"

puts simulators_to_markdown

puts "\n\n## Homebrew Config\n\n"

puts homebrew_to_markdown

